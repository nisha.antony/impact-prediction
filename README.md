# Impact Prediction

Design and implement a big data system for predicting the long-term impact (number of citations or similar metrics) of a recently published scientific work.

## Building model with historic data

We used [AMiner Academic Citation Dataset](https://www.kaggle.com/kmader/aminer-academic-citation-dataset) from Kaggle to train our model. 

We saved the model for citation count prediction of latest works.



