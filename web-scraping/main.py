import prep.data_collection 

search_keywords = ['Economics',
                   'Engineering', 
                   'Computer Science', 
                   'Health & Medical Science', 
                   'Mathematics',
                   'Social Science'
                   ]

for key in search_keywords:
    prep.data_collection.querying_arxiv_api_(key, '100', '5000')