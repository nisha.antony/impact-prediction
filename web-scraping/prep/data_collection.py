import requests
from bs4 import BeautifulSoup
import json

def parse_data_from_arxiv_api(soup):
    output={}
    try:
        output['query']=soup.title.get_text()
        output['title']=soup.find_all('title')[-1].get_text() 
        output['id']=soup.id.get_text()
        output['authors']=[a.get_text().strip('\n') for a in soup.find_all('author')]
        output['published']=soup.published.get_text()
        output['updated']=soup.updated.get_text()
        output['summary']=soup.summary.get_text()
        output['comment']=soup.find('arxiv:comment').get_text() 
        output['href']=soup.find_all('link')[-1]['href']
        
    except Exception as e:
	    return False
    return output

def querying_arxiv_api_(category, start, max_number): 
    papers_scraped={}
    url = 'http://export.arxiv.org/api/query?'
    search_param = 'search_query=all:' + category + '&start=' + start + '&max_results=' + max_number + '&sortBy=submittedDate&sortOrder=descending'
    page_request = requests.get(url + search_param)

    soup = BeautifulSoup(page_request.content, 'html.parser')
        
    entries=[e for e in soup.find_all('entry')]
    print(len(entries))
    for entry in entries:
        parsed_data=parse_data_from_arxiv_api(entry)
        if parsed_data:
            papers_scraped[parsed_data['id']]=parsed_data
            filename = category + '.json'
            with open(filename, 'w') as outfile:
                json.dump(papers_scraped, outfile)
    return



