import pandas as pd
import numpy as np
import json
import pickle
import sklearn
import sys,getopt
import os	

class CitationCount:
    
    def __init__(self, 
                 json_file_path = "Engineering.json", 
                 resources_path = "Resources"):
        self.data = pd.read_json(json_file_path).T
        self.resource_path = resources_path
        self.new_data = None
        self.load_modules()
        
    def load_modules(self):
        self.lda = pickle.load(open(os.path.join(self.resource_path,"lda.sav"), 'rb'))
        self.model = pickle.load(open(os.path.join(self.resource_path,"finalized_model.sav"), 'rb'))
        self.vectorizer = pickle.load(open(os.path.join(self.resource_path,"countvectorizer.sav"), 'rb'))
        
    def transform(self):
        index = self.data["id"].apply(lambda x: x.split("/")[-1])
        years = [int(x) for x in self.data["published"].str[:4].values]
        titles = self.data["title"].str.lower().values
        self.all = pd.DataFrame(index = index)
        self.all["Year"] = years
        self.all["Title"] = titles
        
        countvec = self.vectorizer.transform(self.all["Title"]).todense()
        new_data = self.lda.transform(countvec)
        new_data = np.hstack([new_data, self.all[["Year"]].values])
        return new_data
    
    def predict(self):
        new_data = self.transform()
        self.all["n_citation"] = np.round(np.exp(self.model.predict(new_data)))
        return self.all
    
    
def main(argv):
    
    inputfile = ''
    outputfile = ''
    try:
        opts, args = getopt.getopt(argv,"hi:o:r:",["ifile=","ofile=","rpath="])
    except getopt.GetoptError:
        print('test.py -i <inputfile> -r <resourcepath> -o <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('test.py -i <inputfile> -r <resourcepath> -o <outputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-r", "--rpath"):
            resourcepath = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
    print('Input file is "', inputfile)
    print('Output file is "', outputfile)
    
    c = CitationCount(json_file_path = inputfile, resources_path = resourcepath)
    out = c.predict()
    out.to_csv(outputfile)
    
if __name__ == "__main__":
    main(sys.argv[1:])
    